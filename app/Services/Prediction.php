<?php

namespace App\Services;

use App\Models\Team;

class Prediction
{
    private Team $team;
    private $predictions = [];

    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    public function calculate() : int
    {
        $teams = Team::query()->where('in_league', true)->get()->sortBy([
            ['points', 'desc'],
            ['wins', 'desc'],
            ['goals_different', 'desc']
        ]);

        // I will make it pretty simple for now, but for sure, the logic may to be improved :)
        if ($teams[0]->points == $teams[1]->points && $teams[1]->morePointsWhen($teams[2])) {
            $this->predictions[$teams[0]->id] = 50;
            $this->predictions[$teams[1]->id] = 50;
            $this->predictions[$teams[2]->id] = 0;
            $this->predictions[$teams[3]->id] = 0;
        } elseif ($teams[0]->morePointsWhen($teams[1]) && $teams[1]->morePointsWhen($teams[2])) {
            $predictionFirst = 50 + ($teams[1]->points - $teams[2]->points) * 10;
            $this->predictions[$teams[0]->id] = $predictionFirst;
            $this->predictions[$teams[1]->id] = 100 - $predictionFirst;
            $this->predictions[$teams[2]->id] = 0;
            $this->predictions[$teams[3]->id] = 0;
        } elseif ($teams[0]->morePointsWhen($teams[1]) && $teams[1]->points == $teams[2]->points) {
            $this->predictions[$teams[0]->id] = 50;
            $this->predictions[$teams[1]->id] = 25;
            $this->predictions[$teams[2]->id] = 25;
            $this->predictions[$teams[3]->id] = 0;
        } elseif ($teams[0]->morePointsWhen($teams[1]) && $teams[1]->points == $teams[2]->points && $teams[2]->points == $teams[3]->points) {
            $this->predictions[$teams[0]->id] = 70;
            $this->predictions[$teams[1]->id] = 10;
            $this->predictions[$teams[2]->id] = 10;
            $this->predictions[$teams[3]->id] = 10;
        } elseif ($teams[0]->points == $teams[1]->points && $teams[1]->points == $teams[2]->points && $teams[2]->points == $teams[3]->points) {
            $this->predictions[$teams[0]->id] = 25;
            $this->predictions[$teams[1]->id] = 25;
            $this->predictions[$teams[2]->id] = 25;
            $this->predictions[$teams[3]->id] = 25;
        }

        return $this->predictions[$this->team->id];
    }
}
