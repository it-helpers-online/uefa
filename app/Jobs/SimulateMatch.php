<?php

namespace App\Jobs;

use App\Models\Match;
use App\Models\Team;
use Faker\Provider\Miscellaneous;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SimulateMatch implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Match $match;
    private ?Team $winner;
    private Team $home;
    private Team $guest;

    public function __construct(Match $match)
    {
        $this->match = $match;
        $this->home = $this->match->home;
        $this->guest = $this->match->guest;
    }

    public function handle()
    {
        $chanceWinForHome = 60;

        if ($this->home->power > $this->guest->power) {
            $chanceWinForHome = $chanceWinForHome + ($this->home->power - $this->guest->power);
        }

        if ($this->home->power < $this->guest->power) {
            $chanceWinForHome = $chanceWinForHome - ($this->guest->power - $this->home->power);
        }

        $this->play($chanceWinForHome);
    }

    public function play(int $chanceWinForHome)
    {
        $this->setWinner($chanceWinForHome)
            ->setScores();
    }

    private function setWinner(int $chanceWinForHome)
    {
        $isHomeWinner = Miscellaneous::boolean($chanceWinForHome);

        if ($isHomeWinner) {
            $this->winner = $this->home;

            return $this;
        }

        $isDraws = Miscellaneous::boolean($chanceWinForHome);
        if ($isDraws) {
            $this->winner = null;

            return $this;
        }

        $this->winner = $this->guest;

        return $this;
    }

    private function setScores()
    {
        if ($this->winner == null) {
            $goals = random_int(0, 8);

            $this->match->update(['home_score' => $goals, 'guest_score' => $goals]);

            return $this;
        }

        $homeGoals = random_int(1, 8);
        $guestGoals = $homeGoals - random_int(0, 3);

        if ($guestGoals < 0) {
            $guestGoals = 0;
        }

        $this->match->update(['home_score' => $homeGoals, 'guest_score' => $guestGoals]);

        return $this;
    }
}
