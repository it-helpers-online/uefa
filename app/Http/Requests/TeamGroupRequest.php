<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeamGroupRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'teams' => ['required', 'array', 'size:4'],
            'teams.*' => ['required', 'exists:teams,id']
        ];
    }
}
