<?php

namespace App\Http\Controllers;

use App\Events\TeamGroupCreated;
use App\Http\Requests\TeamGroupRequest;
use App\Models\Match;
use App\Models\Team;

class TeamGroupController extends Controller
{
    public function index()
    {
        $matches = Match::query()->with(['guest', 'home'])->get()->groupBy('date');

        return view('group', compact('matches'));
    }

    public function store(TeamGroupRequest $request)
    {
        Team::query()->whereIn('id', $request->get('teams'))->update(['in_league' => true]);

        event(new TeamGroupCreated($request->get('teams')));

        return redirect()->route('group.index');
    }

    public function destroy()
    {
        Team::query()->update(['in_league' => false]);

        Match::query()->delete();

        return redirect()->route('home');
    }
}
