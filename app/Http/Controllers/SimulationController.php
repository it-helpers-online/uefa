<?php

namespace App\Http\Controllers;

use App\Jobs\SimulateMatch;
use App\Models\Match;
use App\Models\Team;

class SimulationController extends Controller
{
    public function index()
    {
        $teams = Team::query()->where('in_league', true)->get()->sortBy([
            ['points', 'desc'],
            ['wins', 'desc'],
            ['goals_different', 'desc']
        ]);
        $weeklyMatches = Match::query()->with(['guest', 'home'])->whereNull('home_score')->limit(2)->orderBy('date')->get();
        $currentWeek = Match::query()->whereNotNull('home_score')->count() / 2 + 1;
        $matches = collect([]);

        if (request()->get('show_results', false) == true) {
            $matches = Match::all()->sortBy('date');
        }

        return view('simulation', compact('teams', 'weeklyMatches', 'currentWeek', 'matches'));
    }

    public function group()
    {
        Match::query()
            ->whereNull('home_score')
            ->get()
            ->each(fn(Match $match) => dispatch(new SimulateMatch($match)));

        return redirect()->route('simulation.index', ['show_results' => true]);
    }

    public function week()
    {
        $weekDate = now()->setTime(21, 0);

        Match::query()
            ->whereNull('home_score')
            ->whereDate('date', '>=', $weekDate)
            ->limit(2)
            ->get()
            ->each(fn(Match $match) => dispatch(new SimulateMatch($match)));

        return redirect()->route('simulation.index');
    }

    public function reset()
    {
        Match::query()
            ->whereNotNull('home_score')
            ->update(['home_score' => null, 'guest_score' => null]);

        return redirect()->route('simulation.index');
    }
}
