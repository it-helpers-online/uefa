<?php

namespace App\Listeners;

use App\Events\TeamGroupCreated;
use App\Models\Match;

class CreateGroupMatches
{
    public function handle(TeamGroupCreated $event)
    {
        // day 1
        Match::query()->create([
            'home_id'   => $event->teams_id[0],
            'guest_id'  => $event->teams_id[1],
            'date'      => now()->addWeeks(0)->setTime(21, 0)->toDateTimeString()
        ]);

        Match::query()->create([
            'home_id'   => $event->teams_id[2],
            'guest_id'  => $event->teams_id[3],
            'date'      => now()->addWeeks(0)->setTime(21, 0)->toDateTimeString()
        ]);

        // day 2
        Match::query()->create([
            'home_id'   => $event->teams_id[0],
            'guest_id'  => $event->teams_id[2],
            'date'      => now()->addWeeks(1)->setTime(21, 0)->toDateTimeString()
        ]);

        Match::query()->create([
            'home_id'   => $event->teams_id[1],
            'guest_id'  => $event->teams_id[3],
            'date'      => now()->addWeeks(1)->setTime(21, 0)->toDateTimeString()
        ]);

        // day 3
        Match::query()->create([
            'home_id'   => $event->teams_id[0],
            'guest_id'  => $event->teams_id[3],
            'date'      => now()->addWeeks(2)->setTime(21, 0)->toDateTimeString()
        ]);

        Match::query()->create([
            'home_id'   => $event->teams_id[1],
            'guest_id'  => $event->teams_id[2],
            'date'      => now()->addWeeks(2)->setTime(21, 0)->toDateTimeString()
        ]);

        // day 4
        Match::query()->create([
            'home_id'   => $event->teams_id[1],
            'guest_id'  => $event->teams_id[0],
            'date'      => now()->addWeeks(3)->setTime(21, 0)->toDateTimeString()
        ]);

        Match::query()->create([
            'home_id'   => $event->teams_id[3],
            'guest_id'  => $event->teams_id[2],
            'date'      => now()->addWeeks(3)->setTime(21, 0)->toDateTimeString()
        ]);

        // day 5
        Match::query()->create([
            'home_id'   => $event->teams_id[2],
            'guest_id'  => $event->teams_id[0],
            'date'      => now()->addWeeks(4)->setTime(21, 0)->toDateTimeString()
        ]);

        Match::query()->create([
            'home_id'   => $event->teams_id[3],
            'guest_id'  => $event->teams_id[1],
            'date'      => now()->addWeeks(4)->setTime(21, 0)->toDateTimeString()
        ]);

        // day 6
        Match::query()->create([
            'home_id'   => $event->teams_id[3],
            'guest_id'  => $event->teams_id[0],
            'date'      => now()->addWeeks(5)->setTime(21, 0)->toDateTimeString()
        ]);

        Match::query()->create([
            'home_id'   => $event->teams_id[2],
            'guest_id'  => $event->teams_id[1],
            'date'      => now()->addWeeks(5)->setTime(21, 0)->toDateTimeString()
        ]);
    }
}
