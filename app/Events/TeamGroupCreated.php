<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;

class TeamGroupCreated
{
    use Dispatchable;

    public $teams_id;

    public function __construct(array $teams_id)
    {
        $this->teams_id = $teams_id;
    }
}
