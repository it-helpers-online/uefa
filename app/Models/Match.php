<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Match extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = ['id'];

    public function home()
    {
        return $this->belongsTo(Team::class, 'home_id');
    }

    public function guest()
    {
        return $this->belongsTo(Team::class, 'guest_id');
    }
}
