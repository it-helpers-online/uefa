<?php

namespace App\Models;

use App\Services\Prediction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    /** Mutators */
    public function getPointsAttribute()
    {
        return $this->wins * 3 + $this->draws;
    }

    public function getWinsAttribute()
    {
        return Match::query()->where(function ($builder) {
                /** @var Builder $builder */
                $builder->where('home_id', $this->id)->whereColumn('home_score', '>', 'guest_score');
            })->orWhere(function ($builder) {
                /** @var Builder $builder */
                $builder->where('guest_id', $this->id)->whereColumn('guest_score', '>', 'home_score');
            })->count();
    }

    public function getDrawsAttribute()
    {
        return Match::query()->where(function ($builder) {
            /** @var Builder $builder */
            $builder->where('home_id', $this->id)->whereColumn('home_score', '=', 'guest_score');
        })->orWhere(function ($builder) {
            /** @var Builder $builder */
            $builder->where('guest_id', $this->id)->whereColumn('guest_score', '=', 'home_score');
        })->count();
    }

    public function getLossAttribute()
    {
        return Match::query()->where(function ($builder) {
            /** @var Builder $builder */
            $builder->where('home_id', $this->id)->whereColumn('home_score', '<', 'guest_score');
        })->orWhere(function ($builder) {
            /** @var Builder $builder */
            $builder->where('guest_id', $this->id)->whereColumn('guest_score', '<', 'home_score');
        })->count();
    }

    public function getGoalsDifferentAttribute()
    {
        return Match::query()->where('home_id', $this->id)->sum('home_score') +
            Match::query()->where('guest_id', $this->id)->sum('guest_score')
            - Match::query()->where('home_id', $this->id)->sum('guest_score')
            - Match::query()->where('guest_id', $this->id)->sum('home_score');
    }

    public function getProdictionsAttribute()
    {
        $prediction = new Prediction($this);

        return $prediction->calculate();
    }

    public function morePointsWhen(Team $team)
    {
        return $this->points > $team->points;
    }
}
