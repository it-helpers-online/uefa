<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'home')->name('home');
Route::get('group', 'TeamGroupController@index')->name('group.index');
Route::post('group', 'TeamGroupController@store')->name('group.store');
Route::delete('group/regenerate', 'TeamGroupController@destroy')->name('group.reset');
Route::get('simulation', 'SimulationController@index')->name('simulation.index');
Route::post('simulation/group', 'SimulationController@group')->name('simulation.group');
Route::post('simulation/week', 'SimulationController@week')->name('simulation.week');
Route::delete('simulation/reset', 'SimulationController@reset')->name('simulation.reset');
