<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    public function run()
    {
        Team::query()->insert([
            ['name' => 'Arsenal', 'power' => 83],
            ['name' => 'Manchester United', 'power' => 85],
            ['name' => 'Manchester City', 'power' => 88],
            ['name' => 'Chelsea', 'power' => 85],
            ['name' => 'Liverpool', 'power' => 82],
            ['name' => 'Barcelona', 'power' => 90],
            ['name' => 'Real Madrid', 'power' => 90],
            ['name' => 'PSG', 'power' => 90],
            ['name' => 'Bayern Munich', 'power' => 86],
            ['name' => 'Borussia Dortmund', 'power' => 80],
            ['name' => 'Atletico Madrid', 'power' => 80],
            ['name' => 'Juventus', 'power' => 85],
            ['name' => 'Sevilla', 'power' => 80],
            ['name' => 'Roma', 'power' => 80],
            ['name' => 'Tottenham Hotspur', 'power' => 80],
            ['name' => 'Porto', 'power' => 77],
            ['name' => 'Ajax', 'power' => 77],
            ['name' => 'Shahtar Donetsk', 'power' => 75],
            ['name' => 'Villarreal', 'power' => 75],
            ['name' => 'Red Bull Salzburg', 'power' => 65],

            ['name' => 'Napoli', 'power' => 75],
            ['name' => 'Benfica FC', 'power' => 65],
            ['name' => 'Sporting', 'power' => 65],
            ['name' => 'Lazio', 'power' => 70],
            ['name' => 'Zenit', 'power' => 75],
            ['name' => 'CSKA Moscow', 'power' => 75],
            ['name' => 'Spartak Moskow', 'power' => 70],
            ['name' => 'Lokomotiv', 'power' => 70],
            ['name' => 'Dinamo Kiev', 'power' => 80],
            ['name' => 'Milan', 'power' => 80],

            ['name' => 'Valencia', 'power' => 75],
            ['name' => 'Lester City', 'power' => 77],
            ['name' => 'Whest Ham', 'power' => 70],
            ['name' => 'Inter', 'power' => 80],
            ['name' => 'Bazel', 'power' => 65],
            ['name' => 'Dinamo Zagreb', 'power' => 60],
        ]);
    }
}
