<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMatchesTable extends Migration
{
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->id();
            $table->foreignId('home_id')->constrained('teams')->cascadeOnDelete();
            $table->foreignId('guest_id')->constrained('teams')->cascadeOnDelete();

            $table->integer('home_score')->nullable()->index();
            $table->integer('guest_score')->nullable()->index();

            $table->date('date');
        });
    }

    public function down()
    {
        Schema::dropIfExists('matches');
    }
}
