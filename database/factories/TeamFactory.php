<?php

namespace Database\Factories;

use App\Models\Team;
use Illuminate\Database\Eloquent\Factories\Factory;

class TeamFactory extends Factory
{
    protected $model = Team::class;

    public function definition()
    {
        return [
            'name'      => ucwords($this->faker->unique()->words(3, true)),
            'power'     => $this->faker->numberBetween(40, 100),
            'in_league' => $this->faker->boolean
        ];
    }

    public function inLeague()
    {
        return $this->state(function (array $attributes) {
            return [
                'in_league' => true,
            ];
        });
    }
}
