<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class MatchFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'home_id'     => TeamFactory::new()->create()->id,
            'guest_id'    => TeamFactory::new()->create()->id,
            'home_score'  => $this->faker->numberBetween(0, 10),
            'guest_score' => $this->faker->numberBetween(0, 10),
            'date'        => $this->faker->date
        ];
    }

    public function homeWin()
    {
        return $this->state(function (array $attributes) {
            $homeGoals = $this->faker->randomNumber(3, 10);

            return [
                'home_score'  => $homeGoals,
                'guest_score' => $homeGoals - 2,
                'date'        => $this->faker->date
            ];
        });
    }

    public function guestWin()
    {
        return $this->state(function (array $attributes) {
            $guestGoals = $this->faker->randomNumber(3, 10);

            return [
                'home_score'  => $guestGoals - 2,
                'guest_score' => $guestGoals,
                'date'        => $this->faker->date
            ];
        });
    }

    public function draws()
    {
        return $this->state(function (array $attributes) {
            $goals = $this->faker->randomNumber(3, 10);

            return [
                'home_score'  => $goals,
                'guest_score' => $goals,
                'date'        => $this->faker->date
            ];
        });
    }

    public function withoutResults()
    {
        return $this->state(function (array $attributes) {
            return [
                'home_score'  => null,
                'guest_score' => null,
                'date'        => $this->faker->date
            ];
        });
    }
}
