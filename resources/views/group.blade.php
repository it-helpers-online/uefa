@extends('layouts.main')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($matches as $date => $groups)
                <table class="table col-3 ml-5">
                    <thead class="thead-dark">
                    <tr>
                        <th colspan="3" scope="col" style="text-align: center;">Week {{ $loop->index + 1 }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($groups as $match)
                        <tr>
                            <td>{{ $match->home->name }}</td>
                            <td>-</td>
                            <td>{{ $match->guest->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endforeach
        </div>

        <a href="{{ route('simulation.index') }}" class="btn btn-primary">Start simulation</a>
    </div>
@endsection
