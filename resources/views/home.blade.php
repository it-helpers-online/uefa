@extends('layouts.main')

@section('content')
    <div class="jumbotron">
        <h1 class="display-4">Hello, mortal!</h1>
        <p class="lead">Do you want to bet on sports and earn some money? I will help you with this!</p>
        <hr class="my-4">
        <p>Select 4 team in one group which and let's imaging the game!</p>

        <form action="{{ route('group.store') }}" method="post">
            @csrf
            @foreach($teams as $team)
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="{{ $team->id }}" id="team{{ $team->id }}" name="teams[]">
                    <label class="form-check-label" for="team{{ $team->id }}">
                        {{ $team->name }}
                    </label>
                </div>
            @endforeach
            <p class="lead">
                <button class="btn btn-primary btn-lg" type="submit">Let's go!</button>
            </p>
        </form>
    </div>
@endsection
