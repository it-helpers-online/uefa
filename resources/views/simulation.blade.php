@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row">
            <table class="table col-8">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Team Name</th>
                    <th scope="col">P</th>
                    <th scope="col">W</th>
                    <th scope="col">D</th>
                    <th scope="col">L</th>
                    <th scope="col">GD</th>
                </tr>
                </thead>
                <tbody>
                @foreach($teams as $team)
                    <tr>
                        <th scope="row">{{ $team->name }}</th>
                        <td>{{ $team->points }}</td>
                        <td>{{ $team->wins }}</td>
                        <td>{{ $team->draws }}</td>
                        <td>{{ $team->loss }}</td>
                        <td>@if($team->goals_different <= 0) {{ $team->goals_different }} @else +{{ $team->goals_different }} @endif</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if($weeklyMatches->count() > 0)
            <table class="table col-3 ml-5">
                <thead class="thead-dark">
                <tr>
                    <th colspan="3" scope="col" style="text-align: center;">Week {{ $currentWeek }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($weeklyMatches as $match)
                    <tr>
                        <td>{{ $match->home->name }}</td>
                        <td>-</td>
                        <td>{{ $match->guest->name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
        <div class="row">
            @if($weeklyMatches->count() > 0)
                <div class="col-xs-6">
                    <form method="post" action="{{ route('simulation.group') }}" class="col-8 form-check-inline">
                        @csrf
                        <button type="submit" class="btn btn-success">Play all matches</button>
                    </form>
                </div>
                <div class="col-xs-6">
                    <form method="post" action="{{ route('simulation.week') }}" class="col-4">
                        @csrf
                        <button type="submit" class="btn btn-success">Play week matches</button>
                    </form>
                </div>
            @endif

            @if($weeklyMatches->count() == 0)
                    <div class="col-xs-6">
                        <form method="post" action="{{ route('simulation.reset') }}" class="col-8 form-check-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Reset results</button>
                        </form>
                    </div>

                    <div class="col-xs-6">
                        <form method="post" action="{{ route('group.reset') }}" class="col-8 mt-10">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Regenerate group</button>
                    </form>
                    </div>
            @endif
        </div>
    </div>

    @if($currentWeek > 3 && $weeklyMatches->count() > 0)
        <div class="container">
            <div class="row">
                <table class="table col-12">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">Championship Prodictions</th>
                        <th scope="col">%</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($teams as $team)
                        <tr>
                            <th scope="row">{{ $team->name }}</th>
                            <td>{{ $team->prodictions }}%</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

    @if($matches->count() > 0)
        <div class="container">
            <div class="row">
                <table class="table col-6" style="margin-top: 20px;">
                    <thead class="thead-dark">
                    <tr>
                        <th colspan="4" scope="col" style="text-align: center;">All matches</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($matches as $match)
                        @if($loop->index % 2 == 0)
                            <tr>
                                <th colspan="4" scope="col" style="text-align: center;">Week {{ round(($loop->index + 1) / 2) }}</th>
                            </tr>
                        @endif
                        <tr>
                            <td>{{ $match->home->name }}</td>
                            <td>-</td>
                            <td>{{ $match->guest->name }}</td>
                            <td>{{ $match->home_score }} - {{ $match->guest_score }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif

@endsection
