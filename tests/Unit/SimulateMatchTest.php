<?php

namespace Tests\Unit;

use App\Jobs\SimulateMatch;
use App\Models\Match;
use Tests\TestCase;

class SimulateMatchTest extends TestCase
{
    /** @test */
    public function it_can_simulate_match_results()
    {
        $match = Match::factory()->create();

        dispatch(new SimulateMatch($match));

        $this->assertNotNull($match->fresh()->home_score);
        $this->assertNotNull($match->fresh()->guest_score);
    }
}
