<?php

namespace Tests\Unit;

use App\Events\TeamGroupCreated;
use App\Models\Match;
use App\Models\Team;
use Tests\TestCase;

class TeamGroupCreatedTest extends TestCase
{
    /** @test */
    public function it_event_should_create_all_matches_in_group()
    {
        $matches = Team::factory()->count(4)->inLeague()->create();

        event(new TeamGroupCreated($matches->pluck('id')->toArray()));

        $this->assertCount(12, Match::all());
    }
}
