<?php

namespace Tests\Unit;

use App\Models\Match;
use App\Models\Team;
use Tests\TestCase;

class TeamTest extends TestCase
{
    private Team $team;

    protected function setUp(): void
    {
        parent::setUp();

        $this->team = Team::factory()->inLeague()->create();
    }

    /** @test */
    public function it_calculate_0_wins_if_no_played_matches()
    {
        $this->assertEquals($this->team->wins, 0);
    }

    /** @test */
    public function it_calculate_wins_correctly()
    {
        Match::factory()->homeWin()->create(['home_id' => $this->team->id]);
        Match::factory()->guestWin()->create(['guest_id' => $this->team->id]);

        $this->assertEquals(2, $this->team->wins);
    }

    /** @test */
    public function it_calculate_0_draws_if_no_played_matches()
    {
        $this->assertEquals($this->team->draws, 0);
    }

    /** @test */
    public function it_calculate_draws_correctly()
    {
        Match::factory()->draws()->count($count = 2)->create(['home_id' => $this->team->id]);

        $this->assertEquals($count, $this->team->draws);
    }

    /** @test */
    public function it_calculate_0_loss_if_no_played_matches()
    {
        $this->assertEquals($this->team->loss, 0);
    }

    /** @test */
    public function it_calculate_loss_correctly()
    {
        Match::factory()->homeWin()->create(['guest_id' => $this->team->id]);

        $this->assertEquals(1, $this->team->loss);
    }

    /** @test */
    public function it_calculate_goals_differents_correctly()
    {
        $match = Match::factory()->homeWin()->create(['home_id' => $this->team->id]);

        $this->assertEquals($match->home_score - $match->guest_score, $this->team->goals_different);
    }

    /** @test */
    public function it_check_if_more_score_correctly()
    {
        $match = Match::factory()->homeWin()->create(['home_id' => $this->team->id]);

        $this->assertTrue($this->team->morePointsWhen($match->guest));
        $this->assertFalse($match->guest->morePointsWhen($this->team));
    }

    /** @test */
    public function it_calculate_points_correctly_for_win_and_loss()
    {
        $match = Match::factory()->homeWin()->create(['home_id' => $this->team->id]);

        $this->assertEquals(3, $this->team->points);
        $this->assertEquals(0, $match->guest->points);
    }

    /** @test */
    public function it_calculate_points_correctly_for_draws()
    {
        $match = Match::factory()->draws()->create(['home_id' => $this->team->id]);

        $this->assertEquals(1, $this->team->points);
        $this->assertEquals(1, $match->guest->points);
    }
}

