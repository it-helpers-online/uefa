<?php

namespace Tests\Feature;

use App\Models\Team;
use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function it_show_all_teams_on_home_page()
    {
        $teams = Team::factory()->count(10)->create();

        $response = $this->get(route('home'))
            ->assertOk();

        $teams->each(fn(Team $team) => $response->assertSee($team->name));
    }
}
