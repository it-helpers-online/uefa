<?php

namespace Tests\Feature;

use App\Jobs\SimulateMatch;
use App\Models\Match;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class SimulationTest extends TestCase
{
    /** @test */
    public function it_can_simulate_all_group_games()
    {
        $matches = Match::factory()->count(4)->withoutResults()->create();

        Bus::fake();

        $this->post(route('simulation.group'))->assertRedirect();

        Bus::assertDispatchedTimes(SimulateMatch::class, $matches->count());
    }

    /** @test */
    public function it_can_simulate_weekly_games()
    {
        Bus::fake();

        Match::factory()->count(4)->withoutResults()->create(['date' => now()]);
        Match::factory()->count(4)->withoutResults()->create(['date' => now()]);
        Match::factory()->count(4)->withoutResults()->create(['date' => now()->addWeek()]);
        Match::factory()->count(4)->withoutResults()->create(['date' => now()->addWeek()]);

        $this->post(route('simulation.week'))->assertRedirect();

        Bus::assertDispatchedTimes(SimulateMatch::class, 2);
    }

    /** @test */
    public function it_can_reset_all_matches()
    {
        $matches = Match::factory()->count(4)->draws()->create();

        $this->delete(route('simulation.reset'))->assertRedirect();

        $matches->each(function (Match $match) {
            $this->assertNull($match->fresh()->home_score);
            $this->assertNull($match->fresh()->guest_score);
        });
    }
}
