<?php

namespace Tests\Feature;

use App\Events\TeamGroupCreated;
use App\Models\Team;
use Illuminate\Support\Facades\Event;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class GroupTest extends TestCase
{
    /** @test
     * @dataProvider getInvalidGroupCount
     */
    public function it_can_not_create_group_if_teams_count_not_equal_4($invalidTeamCount)
    {
        $this->withoutExceptionHandling()->expectException(ValidationException::class);

        $teams = Team::factory()->count($invalidTeamCount)->create()->pluck('id')->toArray();

        $this->post(route('group.store', compact('teams')));
    }

    /** @test */
    public function it_update_team_in_league_field_if_group_created()
    {
        Event::fake();

        $teams = Team::factory()->count(4)->create()->pluck('id')->toArray();

        $this->post(route('group.store', compact('teams')))
            ->assertRedirect();

        $this->assertTrue(Team::query()->whereIn('id', $teams)->where('in_league', true)->exists());

        Event::assertDispatched(TeamGroupCreated::class);
    }

    /** @test */
    public function it_can_reset_group()
    {
        $teams = Team::factory()->count(4)->inLeague()->create();

        $this->delete(route('group.reset'))
            ->assertRedirect();

        $this->assertTrue(Team::query()->whereIn('id', $teams->pluck('id')->toArray())->where('in_league', false)->exists());
    }

    public function getInvalidGroupCount()
    {
        return [
            [1],
            [2],
            [3],
            [5],
            [6],
        ];
    }
}
